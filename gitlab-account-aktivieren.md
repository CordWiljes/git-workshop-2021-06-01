# 1. Gitlab-Account aktivieren

Wenn Sie bereits am GitLab-Workshop am 10.05. teilgenommen haben oder bereits einen GitLab-Account auf der Installatuon ihrer Institution genutzt haben, können Sie direkt zu Schritt 2 wechseln.

Wahrscheinlich ist bereits ein Account in der GitLab-Instanz Ihrer Organisation für Sie angelegt. Bitten testen Sie dies, indem Sie sich bei Ihrer GitLab-Instanz anmelden, d.h.

* [DFG Schwerpunktprogramm 2207 "Computational Literary Studies" (SPP CLS)](https://cls-gitlab.phil.uni-wuerzburg.de)
* [UMS, Uni Stuttgart](https://clarin06.ims.uni-stuttgart.de) 
 
Als Login können Sie Ihre E-Mail-Adresse verwenden. Alternativ können Sie auch den Username verwenden. Dieser hat voraussichtlich die Form „VornameNachname“.

<!--
![GitLab-Login](images/gitlab-login.png)
-->

Von der Aktivierung der Checkbox „Remember me“ wird aus Sicherheitsgründen abgeraten. Auf keinen Fall dürfen Sie diese Funktion aktivieren, wenn andere Personen Zugriff auf Ihren Rechner haben.
Sollte die Meldung „Invalid login or password“ erscheinen, ist für Sie entweder noch kein Account angelegt, oder Sie haben ein falsches Passwort angegeben. In diesem Fall versuchen Sie bitte zunächst, über den Link „Forgot your password?“ das Passwort zurückzusetzen. 
Sollten Sie daraufhin keine Mail zum Zurücksetzen Ihres Passwortes erhalten, ist unter Ihrer E-Mail noch kein GitLab-Account angelegt. In diesem Fall klicken Sie bitte auf den Link „Register now“ und beantragen Sie einen Account. Sobald Ihr Account von Ihrer/m GitLab-Admin freigeschaltet wurden, werden Sie per E-Mail informiert. Ggf. könnten Sie sich zur Sicherheit bei Ihrer/m Admin melden.
Sollte das Login nicht möglich sein, kontaktieren Sie bitte Ihren lokalen IT-Support.
