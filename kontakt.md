# Kontakt

Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese als Ticket einstellen:

* [SPP CLS](https://cls-gitlab.phil.uni-wuerzburg.de/gitlab-git-schulung/fragen/-/issues)
* [IMS Uni Stuttgart](https://clarin06.ims.uni-stuttgart.de/gitlab-git-schulung/fragen/-/issues) 

stellen. Oder direkt an

Cord Wiljes  
E-Mail: [support@wiljes.de](mailto:support@wiljes.de)  
Tel.: 0521-5217-602  

