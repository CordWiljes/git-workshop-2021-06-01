# Software und Forschungsdaten unter Kontrolle mit Git

Datum: 01.06.2021, 13:00-17:00 Uhr

Herzlich willkommen zum Workshop "Software und Forschungsdaten unter Kontrolle mit Git"!

## Vorbereitung

Zur Vorbereitung auf unseren Workshop stelle ich Ihnen Lernmaterialien zur Verfügung, die Sie dazu anleiten, Git auf Ihrem Rechner zu installieren, ein Projekt einzurichten und erste einfache Befehle auszuführen. Am Ende der Online-Vorbereitung sollten Sie Git auf Ihrem Rechner installiert, ein erstes Projekt via GitLab angelegt und erste einfache Befehle mit git ausgeführt haben. 

1. [Ihren persönlichen GitLab-Account auf dem GitLab Ihrer Institution aktivieren](gitlab-account-aktivieren)
1. [Git auf ihrem likalen Rechner installieren](git-installieren)
1. [Ein erstes Projekt anlegen und klonen](erste-schritte)  
1. [Git Desktop-Client installieren](desktop-client-installieren) (optional)

Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese als [Ticket einstellen](kontakt). Ich werde versuchen, diese dann zeitnah zu beantworten.

Im Workshop werden Sie weitere Git- und GitLab-Funktionalitäten kennenlernen. Gemeinsam werden wir im Workshop Anwendungsbeispiele diskutieren und Übungen zu individuellen Arbeitsabläufe und Best Practices durchführen.

