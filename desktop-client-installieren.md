# 4. Git Desktop-Client installieren (optional)

Dieser Schritt ist optional. Im Workshop wird die Nutzung eines Desktop-Clients vorgestellt. Wenn Sie selbst ausprobieren wollen, installieren Sie diesen bitte im Vorfeld. Sie können auch beide Clients installieren.

Es gibt eine Vielzahl Git-Clients. Empfohlen und im Rahmen des Workshops gezeigt werden:

## SmartGit 

* für Nutzung in Forschung und Lehre kostenlos
* für Windows oder MacOS und Linux

Download unter https://www.syntevo.com/smartgit/

## GitHub Desktop

* kostenlos
* Für Windows, MacOS
* einfach zu nutzen 

Download unter https://desktop.github.com